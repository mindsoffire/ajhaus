import { Component, OnInit } from '@angular/core';

import { Router, ActivatedRoute } from '@angular/router';
import { LibService } from '../../services/lib.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  mylist: any;
  list;
  kycYes;
  constructor(private router: Router, public libSvc: LibService) { }

  async ngOnInit() {
    console.log('ngOnInit in list.ts');
    if (localStorage.getItem('kyc')) this.kycYes = true;
    if (this.libSvc.browseAllFlag) {
      await this.getAllList();
      this.libSvc.browseAllFlag = false;
    }
    this.list = document.querySelector('#listing');
    // this.updateList();
    // this.mylist = await this.libSvc.getList(); console.log({'this.mylist': this.mylist});
    // this.mylist = this.libSvc.listJSON; console.log({'this.mylist': this.mylist});
    // await this.updateList();
    // if (localStorage.getItem('mylist')) this.mylist = await JSON.parse(localStorage.getItem('mylist'));
    // if (this.libSvc.listJSON.clntUserList) this.mylist = this.libSvc.listJSON.clntUserList;
    // this.list.innerHTML = this.libSvc['listJSON'].clntUserList.map(this.createList).join('\n');
    // this.list.innerHTML = this.mylist.map(this.createList).join('\n');
    // this.list.innerHTML = this.libSvc.listJSON.clntUserList.map(this.createList).join('\n');
    // this.list.innerHTML = 'hello';
  }

  showDetails(listId: number, clntAgentCode) {
    // console.log('> listId: %d', listId);
    this.libSvc.listId = listId; this.libSvc.clntAgentCode = clntAgentCode;
    console.log('showDetails in list.ts');
    console.log({ 'listId': this.libSvc.listId, 'clntAgentCode': this.libSvc.clntAgentCode });
    this.router.navigate(['/details']);
  }

  async getAllList() {
    const res = await fetch(`${this.libSvc.SERVER}/api/search-list?crud=getAll`);
    const json = await res.json(); this.mylist = json.clntUserList;
    this.libSvc.searchList = json.clntUserList;
    this.libSvc.searchList.sort((a, b) => { return a.BUA - b.BUA });
    localStorage.setItem('mylist', JSON.stringify(this.libSvc.searchList));
    this.list.innerHTML = this.libSvc.searchList.map(this.createList).join('\n');
  }

  async updateList() {
    const updatelist = this.libSvc.listJSON; console.log({ updatelist });
    this.list.innerHTML = updatelist.clntUserList.map(this.createList).join('\n');
  }


  createList(listItem) {
    return `
    <div class="art">
        <a href = "${listItem.price}" target = "_blank" style="text-decoration: none">
            <h3><large><strong>${listItem.BUA}</strong></large></h3>
            <img class="art-img" src = "{{this.libSvc.SERVER}}/public/images/{{listItem.listPhotoIcon}}">
            <p style="margin-top: 2px"><small>${listItem.district}</small></p>
        </a><hr>
    </div>
    `;
  }

}
