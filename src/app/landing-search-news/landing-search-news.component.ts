import { Component, OnInit } from '@angular/core';

import { Router, ActivatedRoute } from '@angular/router';
import { LibService } from '../services/lib.service';

import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-landing-search-news',
  templateUrl: './landing-search-news.component.html',
  styleUrls: ['./landing-search-news.component.css']
})
export class LandingSearchNewsComponent implements OnInit {

  name = 'AJ\'s AnyObject Library Search';
  searchType = 'Buy'; search = '';
  typeOfProptySelected = 'landed'; districtSelected = 'none'; bedrmsSelected = 'none';

  constructor(private router: Router, public libSvc: LibService) { }
  ngOnInit() {

  }

  async searchAll() {
    this.libSvc.browseAllFlag = false;
    this.libSvc.clntUserSearch.searchType = this.searchType;
    this.libSvc.clntUserSearch.search = this.search;
    this.libSvc.clntUserSearch.typeOfProptySelected = this.typeOfProptySelected;
    this.libSvc.clntUserSearch.districtSelected = this.districtSelected;
    this.libSvc.clntUserSearch.bedrmsSelected = this.bedrmsSelected;
    console.log({ 'libSvc.clntUserList': this.libSvc.clntUserSearch });
    // if (localStorage.getItem('kyc')) this.libSvc.searchList = [];
    const res = this.libSvc.getList(); // console.log({res}); res returns undefined, why?
    // const json = await res.json(); console.log({json})
    this.router.navigate(['/list']);
  }




}
