import { Component, OnInit } from '@angular/core';

import { Router, ActivatedRoute } from '@angular/router';
import { LibService } from '../../services/lib.service';

import { Subscription } from 'rxjs/Subscription';
// import { HttpClient, HttpParams, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
// import 'rxjs/add/operator/take';
// import 'rxjs/add/operator/toPromise';



@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {

  listId = 0; clntAgentCode = '';
  item = [];
  private listId$: Subscription;

  constructor(private router: Router,
    private activatedRoute: ActivatedRoute,
    public libSvc: LibService) { }

  async ngOnInit() {

    // this.listId$ = this.activatedRoute.params.subscribe(
    //   (param) => {
    //     console.log('> param  = ', param);
    //     this.listId = parseInt(param.listId);
    //     this.bookSvc.getBook(this.bookId)
    //       .then((result) => this.book = result)
    //       .catch((err) => {
    //         alert(`Error: ${JSON.stringify(err)}`);
    //       });
    //   }
    // )


    this.listId = this.activatedRoute.snapshot.params['listId'];
    this.clntAgentCode = this.activatedRoute.snapshot.params['clntAgentCode'];
    console.log({ 'params': this.activatedRoute.snapshot.params });
    console.log({ 'params/listId': this.activatedRoute.snapshot.params['listId'] });
    console.log({ 'params/clntAgentCode': this.activatedRoute.snapshot.params['clntAgentCode'] });


    this.libSvc.listId = this.activatedRoute.snapshot.params['listId'] ? this.listId : this.libSvc.listId;
    this.libSvc.clntAgentCode = this.activatedRoute.snapshot.params['clntAgentCode'] ? this.clntAgentCode : this.libSvc.clntAgentCode;

    console.log({ 'listId': this.libSvc.listId, 'clntAgentCode': this.libSvc.clntAgentCode });
    console.log({ 'this.libSvc.searchList': this.libSvc.searchList })

    for (let i = 0; i < this.libSvc.searchList.length; i++) {
      if (this.libSvc.searchList[i].id == this.libSvc.listId && this.libSvc.searchList[i].clntAgentCode == this.libSvc.clntAgentCode) {
        console.log({ i });
        this.item.push(this.libSvc.searchList[i]);
      }
    }
    console.log({ 'item': this.item[0] });

  }




  ngOnDestory() {

  }


}
