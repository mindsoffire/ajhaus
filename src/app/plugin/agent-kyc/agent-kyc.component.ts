import { Component, OnInit, ViewChild } from '@angular/core';

import { VERSION } from '@angular/material';

import { FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import { HttpClient, HttpParams, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import 'rxjs/add/operator/take';
import 'rxjs/add/operator/toPromise';

import { AuthService } from '../../services/auth.service';
import { LibService } from '../../services/lib.service';

export interface Gender {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-agent-kyc',
  templateUrl: './agent-kyc.component.html',
  styleUrls: ['./agent-kyc.component.css']
})
export class AgentKycComponent implements OnInit {
  @ViewChild('proptyPic') proptyPic;

  constructor(
    private router: Router, public tokenSvc: AuthService, public libSvc: LibService, private httpClient: HttpClient
  ) { }

  kyc: any;
  names = [];
  listOwners; crudList; defaultOwner;
  myclntlist = [];

  loggedIn: boolean;
  chgOnePicOnly = false;

  gender: Gender[] = [
    { value: 'M', viewValue: 'Male' },
    { value: 'F', viewValue: 'Female' }
  ];

  async ngOnInit() {
    this.chgOnePicOnly = false;
    this.listOwners = document.querySelector('#listOwners');
    this.crudList = document.querySelector('#crudList');

    (localStorage.getItem('kyc')) ? this.loggedIn = true : this.loggedIn = false;



    if (this.loggedIn) {
      this.kyc = JSON.parse(localStorage.getItem('kyc'));
      console.log({ kyc: this.kyc });
      await this.libSvc.getList();
      localStorage.getItem('ownerlist') ? this.libSvc.ownerList = JSON.parse(localStorage.getItem('ownerlist')) : this.libSvc.ownerList;
      console.log({ 'this.libSvc.ownerList[0].ownerName': this.libSvc.ownerList[0].ownerName });
      // this.defaultOwner = this.libSvc.ownerList[1].ownerName ? this.libSvc.ownerList[1].ownerName : this.libSvc.ownerList[0].ownerName;
      this.defaultOwner = this.libSvc.ownerList[0].ownerName;
      console.log({ 'this.defaultOwner': this.defaultOwner });
      console.log(this.listOwners.value = this.defaultOwner);
      await this.updateCRUDList();
      await this.updateListOwners();
      if (this.kyc.clntCreatDate && this.kyc.clntValKYCDate == null) {
        // copy clntUserKYC into tokenSvc-services-model-storage from localStorage 'kyc' if already logged in & no ajax 'results'
        this.tokenSvc.clntUserKYC = this.kyc;
        if (this.kyc.encLegalNameID) {
          this.names = this.tokenSvc.userNames(this.kyc.encLegalNameID);
          for (let i = 1; i < this.names.length; i++) {
            this.names[0] = this.names[0] + ' ' + this.tokenSvc.rotjaF(this.names[i]);
          }
          this.tokenSvc.clntUserKYC.legalNameID = this.names[0];
        }
        (this.kyc.encEmailID) ? this.tokenSvc.clntUserKYC.emailID = this.tokenSvc.rotjaF(this.kyc.encEmailID) : this.tokenSvc.clntUserKYC.emailID = "..no email.";
        (this.kyc.clntAgentCode) ? this.tokenSvc.clntUserKYC.clntAgentCode = this.kyc.clntAgentCode : this.tokenSvc.clntUserKYC.clntAgentCode = `${this.tokenSvc.clntUserKYC.emailID}@JScorp`;
        if (this.kyc.encEmailID) this.tokenSvc.clntUserKYC.emailID = this.tokenSvc.rotjaF(this.kyc.encEmailID);
        if (this.kyc.encLegalIDCred) this.tokenSvc.clntUserKYC.legalIDCred = this.tokenSvc.rotjaF(this.kyc.encLegalIDCred);
        if (this.kyc.encMobileNumID) this.tokenSvc.clntUserKYC.mobileNumID = this.tokenSvc.rotjaF(this.kyc.encMobileNumID);
        if (this.kyc.address.encPostStreet) this.tokenSvc.clntUserKYC.address.postStreet = this.tokenSvc.rotjaF(this.kyc.address.encPostStreet);
        if (this.kyc.address.encPostUnit) this.tokenSvc.clntUserKYC.address.postUnit = this.tokenSvc.rotjaF(this.kyc.address.encPostUnit);
        if (this.kyc.bank.encBankName) this.tokenSvc.clntUserKYC.bank.bankName = this.tokenSvc.rotjaF(this.kyc.bank.encBankName);
        if (this.kyc.bank.encBankAcct) this.tokenSvc.clntUserKYC.bank.bankAcct = this.tokenSvc.rotjaF(this.kyc.bank.encBankAcct);
        console.log({ 'tokenSvc.clntUserKYC @ agent-kyc': this.tokenSvc.clntUserKYC });
      }
    } else this.router.navigate(['/plugin']);   // route to /plugin compon if 'kyc' not exist in localStorage

  }

  step = 2;
  setStep(index: number) {
    this.step = index;
  }
  nextStep() {
    this.step++;
    this.step++;
  }
  prevStep() {
    this.step--;
    this.step--;
  }

  updateKYC() {
    if (this.tokenSvc.clntUserKYC.legalNameID) {
      this.names = this.tokenSvc.userNames(this.tokenSvc.clntUserKYC.legalNameID);
      for (let i = 1; i < this.names.length; i++) {
        this.names[0] = this.names[0] + ' ' + this.tokenSvc.rotajF(this.names[i]);
      }
      this.tokenSvc.clntUserKYC.encLegalNameID = this.names[0];
    }
    this.tokenSvc.clntUserKYC.encMobileNumID = this.tokenSvc.rotajF(this.tokenSvc.clntUserKYC.mobileNumID);
    console.log({ 'tokenSvc.clntUserKYC @ agent-kyc-update': this.tokenSvc.clntUserKYC });
    fetch(`${this.tokenSvc.SERVER}/api/register-update`, {
      method: 'post',
      headers: { "Content-Type": "application/json; charset=utf-8" },
      // mode: 'cors',
      // cache: 'no-cache',
      body: JSON.stringify(this.tokenSvc.clntUserKYC)
    }).then(result => result.json())
      .catch(err => console.log({ err })).then(result => {
        console.log({ result });
        if (result.clntUserKYC) {
          localStorage.setItem('kyc', JSON.stringify(result.clntUserKYC));
          this.libSvc.getList();
        }
      });
  }

  resetPW() {
    this.tokenSvc.clntUserKYC = {};
    localStorage.removeItem('kyc');
    if (localStorage.getItem('mylist')) localStorage.removeItem('mylist');
    if (localStorage.getItem('myclntlist')) localStorage.removeItem('myclntlist');
    if (localStorage.getItem('ownerlist')) localStorage.removeItem('ownerlist');
    this.router.navigate(['/plugin']);
  }

  async updateCRUDList(source = this.defaultOwner) {
    this.localUrl = '';
    this.chgOnePicOnly = false;
    console.log({ source, 'fetch': `${this.libSvc.SERVER}/api/search-list?crud=${source}` });
    this.defaultOwner = source;
    const res = await fetch(`${this.libSvc.SERVER}/api/search-list?crud=${source}&key1=${this.kyc.encEmailID}&key2=${this.kyc.hshPW}`);
    console.log({ updateCRUDList: res, source });
    const json = await res.json(); this.myclntlist = json.clntUserList;
    this.myclntlist.sort((a, b) => { return a.BUA - b.BUA });
    console.log({ updateCRUDList: json, 'myclntlist': this.myclntlist });
    localStorage.setItem(`myclntlist`, JSON.stringify(this.myclntlist));

    /* this.crudList.innerHTML = json.clntUserList.map(this.createArticle).join('\n');  */
  }

  async updateListOwners() {
    this.listOwners.innerHTML = this.libSvc.ownerList
      .map(owner => `<option value="${owner.ownerName}">${owner.ownerName}</option>`).join('\n');
  }

  localUrl;
  localUrlBack: any[];
  localUrlBank: any[];
  picSrc: string = '';

  showPreviewImage(event: any) {
    if (event.target.files && event.target.files[0] && this.localUrl == '') {
      this.chgOnePicOnly = true;
      const reader = new FileReader();
      reader.onload = (event: any) => {
        this.localUrl = event.target.result;
      };
      reader.readAsDataURL(event.target.files[0]);
    }
  }

  async sendNewPic(propOwnerName, propID) {
    let fileBrowser = this.proptyPic.nativeElement;
    if (fileBrowser.files && fileBrowser.files[0]) {
      let formData = new FormData();
      console.log(fileBrowser.files[0]);
      formData.append('proptyPic', fileBrowser.files[0], `${propOwnerName}-id${propID}`);
      formData.append('encEmailID', this.tokenSvc.clntUserKYC.encEmailID);
      formData.append('hshPW', this.tokenSvc.clntUserKYC.hshPW);
      formData.append('crud', propOwnerName);
      formData.append('propID', propID);
      console.log({ formData });
      console.log({ propOwnerName, 'thisLocalUrl': this.localUrl })
      const res = await fetch(`${this.libSvc.SERVER}/images`, {
        method: 'post',
        // headers: { "Content-Type": "application/json; charset=utf-8" },
        // mode: 'cors',
        // cache: 'no-cache',
        // headers: { "Content-Type": "multipart/form-data; charset=utf-8" },   // let fetch determine for multipart/form-data
        // body: JSON.stringify({ 'clntUserSearch': propOwnerName, 'encEmailID': this.tokenSvc.clntUserKYC.encEmailID, 'hshPW': this.tokenSvc.clntUserKYC.hshPW, 'proptyPic': this.localUrl })
        body: formData
      }).then(result => result.json())
        .catch(err => console.log({ err })).then(result => {
          console.log({ result });
          if (result.clntUserList) {
            this.myclntlist = result.clntUserList;
            this.myclntlist.sort((a, b) => { return a.BUA - b.BUA });
            localStorage.setItem('myclntlist', JSON.stringify(this.myclntlist));
          }

        });
    }
  }

  addToList() {
    this.updateCRUDList(this.libSvc.ownerList[0].ownerName);
  }

  async saveAll(ownerName, del?: boolean, propID?) {
    // this.libSvc.ownerList.length;   
    if (del && ownerName != 'ADD NEW') {
      console.log(`deleting property at propID: ${propID}`);
      for (let i = 0; i < this.myclntlist.length; i++) {
        if (this.myclntlist[i].id == propID) this.myclntlist[i].is_deleted = true;
      }
    } else if (del && ownerName == 'ADD NEW') {
      return;
    }
    if (!del) this.myclntlist[0].ownerName = this.myclntlist[0].ownerName == 'ADD NEW' ? 'You didn\'t change me' : this.myclntlist[0].ownerName;
    console.log({ ownerName, 'this.myclntlist': this.myclntlist });

    return await fetch(`${this.libSvc.SERVER}/api/add-save`, {
      method: 'post',
      headers: { "Content-Type": "application/json; charset=utf-8" },
      mode: 'cors',
      cache: 'no-cache',
      body: JSON.stringify({ 'myclntUpdateList': this.myclntlist, ownerName, 'encEmailID': this.tokenSvc.clntUserKYC.encEmailID, 'hshPW': this.tokenSvc.clntUserKYC.hshPW })
    }).then(result => result.json())
      .catch(err => console.log({ err })).then(async result => {
        console.log({ result });
        this.myclntlist = result.myclntUpdateList;
        this.myclntlist.sort((a, b) => { return a.BUA - b.BUA });
        console.log({ 'myclntlist': this.myclntlist });
        localStorage.setItem(`myclntlist`, JSON.stringify(this.myclntlist));
        await this.libSvc.getList();
        await this.updateListOwners();
        localStorage.getItem('ownerlist') ? this.libSvc.ownerList = JSON.parse(localStorage.getItem('ownerlist')) : this.libSvc.ownerList;
        this.defaultOwner = this.libSvc.ownerList[1].ownerName ? this.libSvc.ownerList[1].ownerName : this.libSvc.ownerList[0].ownerName;
        console.log({ 'this.defaultOwner': this.defaultOwner });
        console.log(this.listOwners.value = this.defaultOwner);
        this.updateCRUDList();
        // await this.updateListOwners();
      });


  }

  showPreviewImageBack(event: any) {
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      reader.onload = (event: any) => {
        this.localUrlBack = event.target.result;
      };
      reader.readAsDataURL(event.target.files[0]);
    }
  }
  showPreviewImageBank(event: any) {
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      reader.onload = (event: any) => {
        this.localUrlBank = event.target.result;
        console.log(JSON.stringify(this.localUrlBank));
        console.log('typeof: ', typeof this.localUrlBank);
      };
      for (let i = 0; i < event.target.files.length; i++) {
        reader.readAsDataURL(event.target.files[i]);
        console.log(JSON.stringify(event.target.files[i]));
      }
    }
  }

  previewFiles(event: any) {
    const preview = document.querySelector('#preview');
    const previewDesc = document.querySelector('#previewDesc');
    const files = event.target.files;

    function readAndPreview(file) {
      // Make sure `file.name` matches our extensions criteria
      if (/\.(jpe?g|png|gif)$/i.test(file.name)) {
        const reader = new FileReader();

        reader.addEventListener(
          'load',
          function () {
            const image = new Image();
            image.height = 100;
            image.title = file.name;
            image.src = this.result;
            preview.appendChild(image); // you can append only proper DOM nodes.
            previewDesc.innerHTML += file.name + '*'; // i added this. Next to try is appendChild(document.createTextNode(file.name))
            // and then appendChild as LI to UL.
          },
          false
        );

        reader.readAsDataURL(file);
      }
    }

    if (files) {
      [].forEach.call(files, readAndPreview); // to check out what's with the empty array calling func readAndPreview on files : file[].
    }
  }



}





