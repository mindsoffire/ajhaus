import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { AotCompiler } from '../../../node_modules/@angular/compiler';

@Injectable()
export class LibService {

    readonly SERVER = this.tokenSvc.SERVER;        // cms server
    // readonly SERVER = 'http://localhost:6700';        // cms server
    // readonly SERVER = 'http://localhost:7700';        // cms server
    // readonly SERVER = 'https://ajhauscms/localtunnel.me';        // cms server

    cursor = 0;
    title = '';
    name = '';
    showcaseTitle = 'SEARCHED LISTING RESULTS';
    clntUserSearch = {
        searchType: '', search: '', typeOfProptySelected: 'none', districtSelected: 'none', bedrmsSelected: 'none'
    }
    listJSON: any; searchList: any; ownerList: any;
    browseAllFlag = false;
    listId = 0; clntAgentCode = '';

    constructor(public tokenSvc: AuthService) { }

    // // GET /list/<listId>
    // getBook(listId: number): Promise<any> {
    //     return (
    //         this.httpClient.get(`${this.SERVER}/book/${bookId}`)
    //             .take(1).toPromise()
    //     );
    // }

    // // GET /books?title=<n>&name=<n>&limit=<n>&offset=<n>
    // getAllBooks(config = {}): Promise<any> {
    //     //Set the query string
    //     let qs = new HttpParams();
    //     qs = qs.set('title', config['title'] || '')
    //         .set('name', config['name'] || '')
    //         .set('limit', config['limit'] || 10)
    //         .set('offset', config['offset'] || 0);
    //     return (
    //         this.httpClient.get(`${this.SERVER}/books`, { params: qs })
    //             .take(1).toPromise()
    //     );
    // }

    async getList() {
        console.log('libSvc.clntUserList @ libSvc :');
        console.log({ 'this.clntUserSearch': this.clntUserSearch, 'this.tokenSvc.clntUserKYC': this.tokenSvc.clntUserKYC });
        return await fetch(`${this.SERVER}/api/search-list?
        searchType=${this.clntUserSearch.searchType}&
        search=${this.clntUserSearch.search}&
        typeOfProptySelected=${this.clntUserSearch.typeOfProptySelected}&
        districtSelected=${this.clntUserSearch.districtSelected}&
        bedrmsSelected=${this.clntUserSearch.bedrmsSelected}&
        key1=${this.tokenSvc.clntUserKYC.encEmailID}&key2=${this.tokenSvc.clntUserKYC.hshPW}`)
            /*         return await fetch(`${this.SERVER}/api/search-list`, {
                        method: 'post',
                        headers: { "Content-Type": "application/json; charset=utf-8" },
                        // mode: 'cors',
                        cache: 'no-cache',
                        body: JSON.stringify({ 'clntUserSearch': this.clntUserSearch, 'encEmailID': this.tokenSvc.clntUserKYC.encEmailID, 'hshPW': this.tokenSvc.clntUserKYC.hshPW }) */
            .then(result => result.json())
            .catch(err => console.log({ err })).then(result => {
                console.log({ result });
                // this.listJSON = result; console.log({ 'this.listJSON': this.listJSON, 'this.listJSON.clntUserList': this.listJSON.clntUserList })
                if (result.clntUserList) {
                    this.searchList = result.clntUserList;
                    this.searchList.sort((a, b) => { return a.BUA - b.BUA });
                    // if (localStorage.getItem('kyc')) {
                    //     localStorage.setItem('myclntlist', JSON.stringify(this.searchList));
                    // } 
                    // else 
                    localStorage.setItem('mylist', JSON.stringify(this.searchList));
                    // console.log('typeOf searchList:', typeof (this.searchList));
                    // console.log('[0]:', this.searchList[0]);
                    // console.log('[1]:', this.searchList[1]);
                }
                if (result.ownerList) {
                    this.ownerList = result.ownerList;
                    this.ownerList.sort((a, b) => {
                        a.ownerName = a.ownerName.toUpperCase(); b.ownerName = b.ownerName.toUpperCase();
                        return (a.ownerName < b.ownerName) ? -1 : (a.ownerName > b.ownerName) ? 1 : 0;
                    });
                    console.log({ 'this.ownerList': this.ownerList });
                    localStorage.setItem('ownerlist', JSON.stringify(this.ownerList));
                }
            });

    }


}