import { Injectable } from '@angular/core';

@Injectable()
export class AuthService {

  constructor() { }

  readonly SERVER = "https://ajbnodez.localtunnel.me";    // to put back after no-cors testing.
  // readonly SERVER = "http://localhost:6700";

  status: string = '';
  nexToken: any;
  clntUserAsset: any;
  clntUserKYC: any = {
    "encEmailID": null, "hshPW": null,
    "subDate": null, "clntCreatDate": null, "clntValKYCDate": null,
    "encLegalNameID": null, "encLegalIDCred": null,
    "photo": {
      "photoIDFrontURL": null, "photoIDBackURL": null, "photoIDsURLsDated": null, "recentFaceVerifiedIDURL": null, "recentFaceVerifiedIDURLDated": null
    },
    "gender": null, "DOB": null, "nationality": null,
    "address": {
      "postCode": null, "encPostStreet": null, "postBlock": null, "encPostUnit": null
    },
    "encMobileNumID": null,
    "mobileNumID": null,
    "bank": {
      "encBankName": null, "bankScanStatemtURL": null, "bankScanStatemtURLDated": null, "encBankScanStatemtBal": null, "encBankAcct": null
    },
    "textAnnotNotaryOthers": null, "clntNotes": null,
    "isDeleted": null
  };


  rotajF(s: string) {
    return s.replace(/[A-Za-z0-9]/g, (c) => {
      return "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890".charAt(
        "MNBVCXZasdfghjklPOIUYTREWQASDFGHJKLmnbvcxzpoiuytrewq6172839405".indexOf(c)
      );
    });
  }
  rotjaF(s: string) {
    return s.replace(/[A-Za-z0-9]/g, (c) => {
      return "MNBVCXZasdfghjklPOIUYTREWQASDFGHJKLmnbvcxzpoiuytrewq6172839405".charAt(
        "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890".indexOf(c)
      );
    });
  }

  sanitizeString(str) {
    str = str.replace(/[^a-z0-9áéíóúñü .-]/gim, "");  /* str = str.replace(/[^a-z0-9áéíóúñü \.,_-]/gim,""); */
    return str.trim();
  }

  userNames(fullName: string): string[] {
    let names = [];
    return names = fullName.split(/[@ ,]+/);
  }

}
